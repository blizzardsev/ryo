import json
import logging
import os
import sys
import traceback
sys.path.insert(0, 'packages')
import asyncio
import boto3
import discord
from discord.errors import *

def generate_api_response(status_code, message):
    """
    Returns a lambda proxy integration-compatible response to be passed back to apigateway shell.

    IN:
        status_code - numerical status code indicating result of request
        message - additional information to return in request
    """
    return {
        "isBase64Encoded": False,
        "statusCode": status_code,
        "headers": {
            "X-Amzn-ErrorType": message
        },
        "body": json.dumps({"message": message})
    }

def parse_request_body(event):
    """
    Parses an incoming proxy event from apigateway, and attempts to get the body object.
    If successful, will then extract request parameters to pass onwards for message delivery.
    Otherwise, returns False to indicate failure.

    IN:
        event - event context (request) from apigateway

    OUT:
        Tuple of (user_id, message) as extracted from request body OR False otherwise
    """
    if event.get("body") is None or event.get("body") == "":
        # Body somehow doesn't exist
        return False

    else:
        request_body = json.loads(event.get("body"))
        # We don't *need* to validate here as API gateway handles this for us, but better safe than sorry
        if (request_body == "" 
            or request_body.get("user_id") is None
            or not isinstance(request_body.get("user_id"), int)
            or request_body.get("character") is None
            or request_body.get("character") == ""
            or not request_body.get("character") in ("m", "n")
            or request_body.get("message") is None
            or request_body.get("message") == ""
            or str.isspace(request_body.get("message"))):
            return False

        return (
            request_body.get("character"),
            request_body.get("user_id"),
            request_body.get("message")
        )

async def send_direct_message(client, user_id, message):
    """
    Sends a direct message to a specific Discord user.
    User must be in at least one common server with the bot.

    IN:
        client - Discord client instance
        user_id - Numerical id of the user to message
        message - Message content
    """
    try:
        # Attempt to get the user, then send the message and close the connection to Discord  
        user = await client.fetch_user(user_id)
        await user.send(message)
        await client.close()
        return generate_api_response(200, "Message sent successfully.")

    except Forbidden:
        # User does not allow unsolicited DMs (or Discord messed up and needs bot token refreshing); close connection to Discord
        await client.close()
        return generate_api_response(403, "This user does not allow unsolicited DM, or bot token requires refreshing.")

    except NotFound:
        # User does not exist; close connection to Discord  
        await client.close()
        return generate_api_response(404, "Could not find user, or user does not exist.")

async def main(event):
    """
    Main event handler.
    This is run via asynchio to allow async operation.
    """
    try:
        _CHARACTER_TOKEN_MAP = {
            "m": os.environ['MONIKA_DISCORD_TOKEN'],
            "n": os.environ['NATSUKI_DISCORD_TOKEN']
        }

        # Validate the request body
        if not parse_request_body(event):
            return generate_api_response(400, "Invalid request body, or missing/empty fields.")

        else:
            # Send the message
            client = discord.Client()
            request_data = parse_request_body(event)
            await client.login(_CHARACTER_TOKEN_MAP.get(request_data[0]))
            return await send_direct_message(client, request_data[1], request_data[2])

    except:
        # Notify alert topic on failure and raise the exception
        logging.error('Failed to push message to Discord; an exception occurred.')
        logging.error(f'Exception info: {traceback.format_exc()}')
        boto3.client('sns').publish(TopicArn=notification_topic_arn,
            Subject='ryo failed: could not deliver message.',
            Message=traceback.format_exc(),
            MessageAttributes={
                'DiscordDisplayAllData': {
                    'DataType': 'String',
                    'StringValue': 'true'
                },
                'EventJson': {
                    'DataType': 'String',
                    'StringValue': json.dumps(event)
                }
            })
        return generate_api_response(500, "Failed to post to Discord; contact maintainer.")

def lambda_handler(event, context):
    """
    Lambda entry point; receives incoming API requests and delivers private messages to Discord users.
    We pass off via asynchio to allow subsequent executions without loop ends.

    IN:
        event - The apigateway event that triggered this execution
        context - Surrounding AWS context
    """
    # Perform setup
    global notification_topic_arn
    logging.getLogger().setLevel(os.environ["LOGGER_LEVEL"])
    notification_topic_arn = os.environ['NOTIFY_TOPIC_ARN']

    # Off we go
    return asyncio.run(main(event))
