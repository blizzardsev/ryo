# About ryo

Ryo is a simple Python-based serverless function for receiving API requests, parsing them and delivering direct Discord messages to users based on that request.

## CI/deployment script configuration

### Dependencies

* **AWS CLI**: Command-line toolkit for AWS @ https://aws.amazon.com/cli/
* **AWS SAM CLI**: Command-line toolkit for AWS SAM @ https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html
* **pigar**: Automatic Python requirements.txt @ https://github.com/damnever/pigar

## Python configuration

### Runtime

* Python 3.8

### Dependencies

* Boto3: AWS toolkit @ https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html
* discordpy: Python Discord API @: https://discordpy.readthedocs.io/en/stable/

### Environment variables

* **ALERT_TOPIC_ARN** : The ARN of the alert topic to send notifications to in the event of controlled failure.
* **DISCORD_BOT_TOKEN** : The Discord bot token used to authenticate against Discord to send messages.
* **LOGGER_LEVEL** : The minimum log level to output on CloudWatch logs.

## Deployment

Use script *deploy_to_dev_stack.bat* *version-number* *bot-token* for manual deployments; E.G `deploy_to_dev_stack.bat 1.0.0 ABC123`. Execute within Ryo directory.
Release deployments are handled by CI.

## Contact

Please raise any suggestions, etc. using the Issue Tracker.

Email: *blizzardsev.dev@gmail.com*
