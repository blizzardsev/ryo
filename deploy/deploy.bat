@echo off
Rem This script runs the sam deploy command within the main script.
Rem This prevents the script automatically closing, since sam deploy produces a return code,
Rem which ends execution.
sam deploy ^
--template-file sam.yml ^
--stack-name dev-ryo-stack ^
--region eu-west-1 ^
--s3-bucket blizzardsev.store.deploy ^
--capabilities CAPABILITY_NAMED_IAM ^
--no-fail-on-empty-changeset ^
--parameter-overrides DeployStage=dev Version=%1 MonikaDiscordToken=%2 NatsukiDiscordToken=%3 > deploy_output.txt
